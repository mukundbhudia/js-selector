var test$ = function() {
window.answerList = {
  A: $("div"),
  B: $("img.some_class"),
  C: $("#some_id"),
  D: $(".some_class"),
  E: $("input#some_id"),
  F: $("div#some_id.some_class"),
  G: $("div.some_class#some_id"),
  H: $(".some_other_class"),
  I: $(".some_class.some_other_class"),
  J: $(".some_class.some_other_class.missing_class"),
  K: $(".some_other_class.test_class"),
  L: $(".some_other_class.some_other_class"),
  M: $(".some_other_class.some_other_class.some_other_class"),
  N: $("img.some_class.some_other_class"),
  O: $("#some_other_id.some_class.some_other_class"),
  P: $("#some_id.some_class"),
  Q: $("#some_id.test_class"),
  R: $("#some_id.some_class.test_class"),
  S: $("#some_other_id.some_class.test_class"),
  T: $("#some_other_id.some_class.some_other_class"),
  U: $("div.some_class.some_other_class"),
  V: $("div.test_class"),
};

var expectedResult = {
  A: {
    DIV: 2
  },
  B: {
    IMG: 1
  },
  C: {
    DIV: 1
  },
  D: {
    DIV: 1,
    IMG: 1
  },
  E: {
  },
  F: {
    DIV: 1
  },
  G: {
    DIV: 1
  },
    H: {
    DIV: 1,
    IMG: 1
  },
    I: {
    DIV: 1,
    IMG: 1
  },
    J: {
    DIV: 0,
    IMG: 0
  },
    K: {
    DIV: 0,
    IMG: 1
  },
    L: {
    DIV: 1,
    IMG: 1
  },
    M: {
    DIV: 1,
    IMG: 1
  },
    N: {
    DIV: 0,
    IMG: 1
  },
    O: {
    DIV: 0,
    IMG: 1
  },
    P: {
    DIV: 1,
    IMG: 0
  },
    Q: {
    DIV: 0,
    IMG: 0
  },
    R: {
    DIV: 0,
    IMG: 0
  },
    S: {
    DIV: 0,
    IMG: 1
  },
    T: {
    DIV: 0,
    IMG: 1
  },
    U: {
    DIV: 1,
    IMG: 0
  },
    V: {
    DIV: 0,
    IMG: 0
  },
  questions: 22
}


var computeString = function(result) {
  var returnArray = [];

  var divs = result["DIV"] || 0;
  var imgs =  result["IMG"] || 0;

  if (divs === 1) returnArray.push(divs + " DIV");
  else returnArray.push(divs + " DIVs");

  if (imgs === 1) returnArray.push(imgs + " IMGs");
  else returnArray.push(imgs + " IMGs");

  return returnArray.join(", ");
}

var testsPassed = 0;

for (answerName in answerList){
  var answer = answerList[answerName], i = 0, ii = answer.length, tagList = {};

  for (; i < ii; i++) {
    var answerTag = answer[i].tagName;
    if (tagList[answerTag]) tagList[answerTag]++;
    else tagList[answerTag] = 1;
  }

  var expected = computeString(expectedResult[answerName]);
  var found = computeString(tagList);
  var result = (expected === found) ? "Yes" : "No";
  if (result === "Yes") testsPassed++;

  console.log("\n------------------------\n\nAnswer", answerName);
  console.log("Expected:", expected);
  console.log("Found:", found);
  console.log("Correct:", result);

}

console.log("\n------------------------\n\nTests Passed:", testsPassed, "of", expectedResult.questions ); 
}