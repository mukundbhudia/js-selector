/**
 * Selector function.
 * Input a CSS selector as a string and receive an array of matching HTML DOM elements.
 * 
 * @param {string}      selector
 * @returns {array}     HTML dom elements
 */
var $ = function (selector) {
    //Persist an array of all elements we encounter that match the selector string
    var matchedElements = [];
    //For each selector string we persist a map. This will help orchestrate finding
    //each element mentioned in the selector string
    var selectorMap = {
        "tag": null,
        "id": null,
        "classes": []
    };

    //To identify particular elements of the given input selector string, the input
    //is split by '.' for a class attribute, or '#' for an id attribute
    var splitSelectorString = selector.split(/[\.#\s]/);

    //For every element name we determine the type and populate it into the map
    for (var i = 0; i < splitSelectorString.length; i++) {
        if (splitSelectorString[i] !== "") {
            var elementType = determineElementType(splitSelectorString[i]);

            if (elementType === 'tag') {
                selectorMap.tag = splitSelectorString[i];
            } else if (elementType === 'id') {
                selectorMap.id = splitSelectorString[i];
            } else if (elementType === 'class') {
                selectorMap.classes.push(splitSelectorString[i]);
            } else {
                //If an element name cannot be classified then it matches with nothing and
                //we can therefore return an empty array.
                return [];
            }
        }
    }

    var elementFoundById = document.getElementById(selectorMap.id);
    var elementsFoundByTag = document.getElementsByTagName(selectorMap.tag);

    //The selector string contains a tag, id and class(es)
    if (selectorMap.tag !== null && selectorMap.id !== null && selectorMap.classes.length > 0) {
        if (elementFoundById.tagName.toLowerCase() === selectorMap.tag &&
        doClassesMatch(selectorMap.classes, elementFoundById.classList)) {
            matchedElements.push(elementFoundById);
        }
    //The selector string contains a tag, id and no class(es)
    } else if (selectorMap.tag !== null && selectorMap.id !== null && selectorMap.classes.length === 0) {
        if (elementFoundById.tagName.toLowerCase() === selectorMap.tag) {
            matchedElements.push(elementFoundById);
        }
    //The selector string contains a tag, no id and class(es)
    } else if (selectorMap.tag !== null && selectorMap.id === null && selectorMap.classes.length > 0) {
        for (var j = 0; j < elementsFoundByTag.length; j++) {
            if (true === doClassesMatch(selectorMap.classes, elementsFoundByTag[j].classList)) {
                matchedElements.push(elementsFoundByTag[j]);
            }
        }
    //The selector string contains just a tag
    } else if (selectorMap.tag !== null && selectorMap.id === null && selectorMap.classes.length === 0) {
        matchedElements = elementsFoundByTag;
    //The selector string contains just an id
    } else if (selectorMap.tag === null && selectorMap.id !== null && selectorMap.classes.length === 0) {
        matchedElements.push(elementFoundById);
    //The selector string contains no tag an id and class(es)
    } else if (selectorMap.tag === null && selectorMap.id !== null && selectorMap.classes.length > 0) {
        // elements.push(elementById);
            if (true === doClassesMatch(selectorMap.classes, elementFoundById.classList)) {
                matchedElements.push(elementFoundById);
            }
    //The selector string contains just class(es)
    } else if (selectorMap.tag === null && selectorMap.id === null && selectorMap.classes.length > 0) {
        //We get all the elements that match the first class in the selector because the 
        //array of classes from this first match are equal to or less than the total classes
        //that will match the entire class selector
        var elementClasses = document.getElementsByClassName(selectorMap.classes[0]);
        //for every element found we get all classes attributed to it
        for (var k = 0; k < elementClasses.length; k++) {
            var elementClassesList = elementClasses[k].classList;
            //We check that all selector classes appear in the element class list
            if (doClassesMatch(selectorMap.classes, elementClassesList) === true) {
                //...then we can add the element into the array
                matchedElements.push(elementClasses[k]);
            }
        }
    }
    return matchedElements;
};

/**
 * Determines the type of DOM element given from the string name
 * Additionally this function also checks the existence of each element given  
 * 
 * @param {string}      element     This is the element name
 * @returns {string}                Whether the element is an id, tag or a class
 */
var determineElementType = function (element) {
    if (document.getElementById(element) !== null) {
        return "id";
    } else if (document.getElementsByTagName(element).length > 0) {
        return "tag";
    } else if (document.getElementsByClassName(element).length > 0) {
        return "class";
    } else {
        console.warn("Unknown element type: " + element);
        return undefined;
    }
};

/**
 * Checks if every class in the selector string appears in the list of
 * classes per each DOM element. All elements in selectorClasses must be
 * in elementClasses, but elementClasses may contain more elements. This
 * illustrates a HTML element having more class attributes than mentioned
 * in the selector string.
 * 
 * @param {array} selectorClasses   Array of classes inputted as a selector string
 * @param {array} elementClasses    Array of classes attributed to an HTML DOM element
 * @returns {boolean}               Whether all elements in selectorClasses are in elementClasses
 */
var doClassesMatch = function (selectorClasses, elementClasses) {
    var matchesFound = 0;
    for (var i = 0; i < elementClasses.length; i++) {
        for (var j = 0; j < selectorClasses.length; j++) {
            //We count the number of matches of classes in both arrays
            if (elementClasses[i] === selectorClasses[j]) {
                matchesFound++;
            }
        }
    }
    //If we find the same number of classes in the selector string as we do in the
    //array of class lists per element, then we've matched an element to the selector
    return matchesFound === selectorClasses.length;
};